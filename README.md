
## get_latest_xml_and_text_files

*Note*: this workflow combines [create_new_export](https://gitlab.com/closer-cohorts1/archivist_export/create_new_export), [download_xml](https://gitlab.com/closer-cohorts1/archivist_export/download_xml) and [download_text_files](https://gitlab.com/closer-cohorts1/archivist_export/download_text_files)


Loop over all prefix from Prefixes_to_export.txt
- [create_new_export](https://gitlab.com/closer-cohorts1/archivist_export/create_new_export): click CREATE NEW EXPORT button
- [download_xml](https://gitlab.com/closer-cohorts1/archivist_export/download_xml): download the newly created xml file
  - Check if any xml files contain missing question/statement literal
  - Clean text within xml
  - Remove text <duplicate [n]> from sequence labels
- [download_text_files](https://gitlab.com/closer-cohorts1/archivist_export/download_text_files): Export text files from archivist
  - Question
    - QV (Question Variables) qv.txt, rename prefix.qvmapping.txt
    - TQ (Topic Questions) tq.txt, rename prefix.tqlinking.txt
  - Datasets
    - TV: tv.txt, rename: prefix.tvlinking.txt
    - DV: dv.txt, rename: prefix.dv.txt

