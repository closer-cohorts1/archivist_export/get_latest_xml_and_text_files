#!/usr/bin/env python3

"""
Python 3: Web scraping using selenium
    - Download tv.txt and dv.txt files from Archivist Datasets, rename: prefix.tvlinking.txt, prefix.dv.txt
    - QV (Question Variables) qv.txt, rename prefix.qvmapping.txt
    - TQ (Topic Questions) tq.txt, rename prefix.tqlinking.txt
"""

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from urllib.parse import urlsplit, urlunsplit

from mylib import get_driver, url_base, archivist_login, get_instrument_url



import pandas as pd
import requests
import time
import sys
import os

n = 5


def download_link(url_location, output_file):
    """
    Given a URL, using requests library to download it to output_file
    """
    print("Download: " + url_location)
    url_get = requests.get(url_location)
    url_get.raise_for_status()
    with open(output_file, "wb") as f: 
        f.write(url_get.content)


def archivist_download_txt(df, main_dir, uname, pw):
    """
    Loop over prefix, downloading txt files
    """
    df_base_url = get_instrument_url(df)
    export_name = pd.Series(df_base_url.base_url.values, index=df.Instrument).to_dict()
    print("Got {} prefix names".format(len(export_name)))
    print(export_name)

    k = -1
    pre_url = None
    for prefix, url in export_name.items():
        prefix = prefix.strip()

        split_url = urlsplit(url)

        output_prefix_dir = os.path.join(main_dir, split_url.netloc.split('.')[0], "txt_by_prefix")
        if not os.path.exists(output_prefix_dir):
            os.makedirs(output_prefix_dir)
        output_type_dir = os.path.join(main_dir, split_url.netloc.split('.')[0], "txt_by_type")
        if not os.path.exists(output_type_dir):
            os.makedirs(output_type_dir)

        text_list_file = os.path.join(os.path.dirname(output_prefix_dir), "text_list.csv")
        if not os.path.isfile(text_list_file) :
            with open(text_list_file, "a") as f:
                f.write( ",".join(["ID","Prefix","Study","Datasets","Dataset_ID", "Dataset_type", "Dataset_link"]) + "\n")


        def log_to_csv(out_file, out_list):
            """append a line to spreadsheet with three values"""
            with open(out_file, "a") as f:
                f.write( ",".join(out_list) + "\n")


        # download instrument: tqlinking, qvmapping
        # https://closer-archivist.herokuapp.com/admin/instruments/PREFIX/exports

        output_dir = os.path.join(output_prefix_dir, prefix)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_tq_dir = os.path.join(output_type_dir, "tqlinking",)
        if not os.path.exists(output_tq_dir):
            os.makedirs(output_tq_dir)

        output_qv_dir = os.path.join(output_type_dir, "qvmapping",)
        if not os.path.exists(output_qv_dir):
            os.makedirs(output_qv_dir)

        output_tv_dir = os.path.join(output_type_dir, "tvlinking",)
        if not os.path.exists(output_tv_dir):
            os.makedirs(output_tv_dir)

        output_dv_dir = os.path.join(output_type_dir, "dv",)
        if not os.path.exists(output_dv_dir):
            os.makedirs(output_dv_dir)

        k = k + 1
        print("Building a new driver")
        driver = get_driver()

        try:
            base_url = split_url.scheme + '://' + split_url.netloc
            print("Logging into host {}".format( base_url ))
            ok = archivist_login(driver, base_url, uname, pw)
            if not ok:
                print(f"Failed to login to {url}: skipping")
                continue
            print(f"Logged into prefix number {k}: {prefix}")

            print("Load 'Instrument' page : " + url)
            driver.get(url)
            delay = 5*n   #seconds
            # find the input box
            inputElement = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//input[@placeholder='Search by id (press return to perform search)']")))
            print("Page is ready!")

            print('Search prefix "{}"'.format(prefix))
            inputElement.send_keys(prefix)

            # choose from dropdown to display all results on one page
            select = Select(driver.find_element(By.XPATH, "//select[@aria-label='rows per page']"))

            # select by visible text
            select.select_by_visible_text('All')

            # locate id and link
            #TODO: make this better
            trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")

            print("This page has {} rows, searching for matching row".format(len(trs)))
            matching = []
            for trtmp in trs:
                if prefix == trtmp.find_elements(By.XPATH, "td")[1].text:
                    matching.append(trtmp)

            if len(matching) == 0:
                log_to_csv(prefix, "n/a", "Could not find a row matching the prefix from instrument page", text_list_file)
                tr = None

            elif len(matching) == 1:
                tr = matching[0]

            else:  # len(matching) > 1:
                log_to_csv(prefix, "n/a", "There was more than one row matching this prefix from instrument page: will download first", text_list_file)
                tr = matching[0]

            # if no instrument found, move to download dataset only
            if tr is None:
                print("No instrument found for".format(prefix))
                dataset_url = base_url + '/admin/datasets/'
                print("Load 'Admin Datasets' page : " + dataset_url)
                driver.get(dataset_url)
                delay = 5*n

                # find the input box
                inputElement = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//input[@placeholder='Search by id (press return to perform search)']")))
                print("Dataset search page is ready!")

                print('Search prefix "{}"'.format(prefix))
                inputElement.send_keys(prefix)

                # choose from dropdown to display all results on one page
                select = Select(driver.find_element(By.XPATH, "//select[@aria-label='rows per page']"))

                # select by visible text
                select.select_by_visible_text('All')

                # locate id and link
                trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")

                print("This dataset page has {} rows, searching for matching row".format(len(trs)))
                matching1 = []
                for trtmp in trs:
                    matching.append(trtmp)

                if len(matching1) == 0:
                    log_to_csv(prefix, "n/a", "Could not find a row matching the prefix from dataset page", text_list_file)
                    tr1 = None
                elif len(matching1) == 1:
                    tr1 = matching1[0]
                else:  # len(matching1) > 1:
                    log_to_csv(prefix, "n/a", "There was more than one row matching this prefix from dataset page", text_list_file)
                    tr1 = None

                if tr1 is None:
                    continue

                # go to individual dataset page
                # column 1 is "ID"
                tr_id = tr1.find_elements(by=By.XPATH, value="td")[0].text
                print(tr_id)

                # datasets page
                data_export_page_url = os.path.join( base_url, 'admin/datasets', tr_id, "exports")
                print("Loading <dataset exports> page for " + prefix)
                print(data_export_page_url)
                driver.get(data_export_page_url)
                time.sleep( 5*n ) # seconds

                # Topic Variables
                print("Getting tv.txt")
                tv_href = driver.find_element(By.XPATH,  "//a[contains(@href, 'tv.txt')]")
                tv_location = tv_href.get_attribute("href")
                # print(tv_location)

                out_tv_instrument = os.path.join(output_dir, prefix + ".tvlinking.txt")
                out_tv_type = os.path.join(output_tv_dir, prefix + ".tvlinking.txt")

                tv_get = requests.get(tv_location)
                tv_get.raise_for_status()
                with open(out_tv_instrument, "wb") as f:
                    f.write(tv_get.content)
                with open(out_tv_type, "wb") as f:
                    f.write(tv_get.content)

                # Derived Variables
                print("Getting dv.txt")
                dv_href = driver.find_element(By.XPATH,  "//*[contains(@href, 'dv.txt')]")
                dv_location = dv_href.get_attribute("href")
                # print(dv_location)

                out_dv_instrument = os.path.join(output_dir, prefix + ".dv.txt")
                out_dv_type = os.path.join(output_dv_dir, prefix + ".dv.txt")

                dv_get = requests.get(dv_location)
                dv_get.raise_for_status()
                with open(out_dv_instrument, "wb") as f:
                    f.write(dv_get.content)
                with open(out_dv_type, "wb") as f:
                    f.write(dv_get.content)

                continue

            # else we did get instrument, so takes the exports from it
            print("did find instrument")
            # column 1 is "ID"
            tr_id = tr.find_elements(by=By.XPATH, value="td")[0].text

            # column 2 is "Prefix"
            tr_prefix = tr.find_elements(by=By.XPATH, value="td")[1].text

            # column 3 is "Study"
            tr_study = tr.find_elements(by=By.XPATH, value="td")[2].text

            # column 4 is "Datasets"
            tr_data = tr.find_elements(by=By.XPATH, value="td")[3]
            tr_data_name = tr_data.text

            # Datasets:
            dataset_list = []
            for d in tr_data_name.split('\n'):
                print(d)
                if d == '':
                    log_to_csv(text_list_file, [tr_id, tr_prefix, tr_study, '', '', ''])
                else:
                    url1 = tr_data.find_element(By.LINK_TEXT, d).get_attribute('href')
                    print(url1)
                    data_id = url1.split('/')[-1]
                    #log_to_csv(text_list_file, [tr_id, tr_prefix, tr_study, d, data_id, url1])
                    dataset_list.append([d, data_id])

            for [dataset_prefix, dataset_id] in dataset_list:
                # datasets page
                data_export_page_url = os.path.join( base_url, 'admin/datasets', dataset_id, "exports")
                print("Loading <dataset exports> page for " + dataset_prefix)
                print(data_export_page_url)
                driver.get(data_export_page_url)
                time.sleep( 5*n ) # seconds

                # Topic Variables
                print("Getting tv.txt")
                tv_href = driver.find_element(By.XPATH,  "//a[contains(@href, 'tv.txt')]")
                tv_location = tv_href.get_attribute("href")
                log_to_csv(text_list_file, [tr_id, tr_prefix, tr_study, dataset_prefix, dataset_id, 'tv', tv_location])

                out_tv_instrument = os.path.join(output_dir, dataset_prefix + ".tvlinking.txt")
                out_tv_type = os.path.join(output_tv_dir, dataset_prefix + ".tvlinking.txt")

                tv_get = requests.get(tv_location)
                tv_get.raise_for_status()
                with open(out_tv_instrument, "wb") as f:
                    f.write(tv_get.content)
                with open(out_tv_type, "wb") as f:
                    f.write(tv_get.content)

                # Derived Variables
                print("Getting dv.txt")
                dv_href = driver.find_element(By.XPATH,  "//*[contains(@href, 'dv.txt')]")
                dv_location = dv_href.get_attribute("href")
                log_to_csv(text_list_file, [tr_id, tr_prefix, tr_study, dataset_prefix, dataset_id, 'dv', dv_location])

                out_dv_instrument = os.path.join(output_dir, dataset_prefix + ".dv.txt")
                out_dv_type = os.path.join(output_dv_dir, dataset_prefix + ".dv.txt")

                dv_get = requests.get(dv_location)
                dv_get.raise_for_status()
                with open(out_dv_instrument, "wb") as f:
                    f.write(dv_get.content)
                with open(out_dv_type, "wb") as f:
                    f.write(dv_get.content)

            print("find link")
            # when exist instrument, get tq and qv
            exports_page_url = os.path.join(os.path.dirname(url), "instruments", prefix, "exports")

            print("***********Loading <instrument exports> page for " + prefix)
            print(exports_page_url)
            driver.get(exports_page_url)
            delay = 5*n # seconds

            myEle = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//h2[text()='TQ']")))
            print("Instrument Export Page is ready!")

            # Topic Questions
            print("Getting tq.txt")
            tq_href = driver.find_element(By.XPATH,  "//*[contains(@href, 'tq.txt')]")
            tq_location = tq_href.get_attribute("href")
            log_to_csv(text_list_file, [tr_id, tr_prefix, tr_study, '', '', 'tq', tq_location])

            out_tq_instrument = os.path.join(output_dir, prefix + ".tqlinking.txt")
            out_tq_type = os.path.join(output_tq_dir, prefix + ".tqlinking.txt")

            tq_get = requests.get(tq_location)
            tq_get.raise_for_status()
            with open(out_tq_instrument, "wb") as f:
                f.write(tq_get.content)
            with open(out_tq_type, "wb") as f:
                f.write(tq_get.content)

            # Question Variables
            print("Getting qv.txt")
            qv_href = driver.find_element(By.XPATH,  "//*[contains(@href, 'qv.txt')]")
            qv_location = qv_href.get_attribute("href")
            log_to_csv(text_list_file, [tr_id, tr_prefix, tr_study, '', '', 'qv', qv_location])

            out_qv_instrument = os.path.join(output_dir, prefix + ".qvmapping.txt")
            out_qv_type = os.path.join(output_qv_dir, prefix + ".qvmapping.txt")

            qv_get = requests.get(qv_location)
            qv_get.raise_for_status()
            with open(out_qv_instrument, "wb") as f:
                f.write(qv_get.content)
            with open(out_qv_type, "wb") as f:
                f.write(qv_get.content)


        except TimeoutException as e:
            print(f"Loading/downloading took too much time.  Error type: {type(e)}")
            print(e)
        finally:
            # succeeded or failed, we quit and make a new driver next iteration
            driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    # Hayley's txt as dataframe
    df = pd.read_csv("Prefixes_to_export.txt", sep="\t")
    main_dir = "export_txt"

    # get all text files
    archivist_download_txt(df, main_dir, uname, pw)


if __name__ == "__main__":
    main()
